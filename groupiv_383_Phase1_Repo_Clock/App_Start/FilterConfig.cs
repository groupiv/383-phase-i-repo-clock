﻿using System.Web;
using System.Web.Mvc;

namespace groupiv_383_Phase1_Repo_Clock
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}